import { Component } from '@angular/core';
import { Platform, ToastController, IonList } from '@ionic/angular';
import { RestProvider } from "../providers/rest/rest";
@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  items = [];
 
  newItem = [];


  constructor(private plt: Platform, private toastController: ToastController, private global: RestProvider) {
    this.plt.ready().then(() => {
      this.loadItems();
    });
  }
 
  // CREATE
  addItem() {
    
 
  }
 
  // READ
  loadItems() {
    this.global.getModel('/posts').then((result) => {
     
      console.log(result)
      
      
       
  }, (err) => {
    console.log(err);
   
  });
  }
 
  // UPDATE
  updateItem(item: any) {
   
  }
 
  // DELETE
  deleteItem(item: any) {
  
      this.showToast('Item removed!');
      
  }
 
  // Helper
  async showToast(msg) {
    const toast = await this.toastController.create({
      message: msg,
      duration: 2000
    });
    toast.present();
  }
}
