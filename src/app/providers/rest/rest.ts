import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { HttpHeaders } from "@angular/common/http";
import { LoadingController } from "@ionic/angular";

const httpOptionsDefault = {
  headers: new HttpHeaders({
    Accept: "application/json"
  })
};

@Injectable()
export class RestProvider {
  baseUrl: String = "";
  apiUrl: String = "";
  ModelId;
  Model: any = {};
  type: String;
  loader: any;
  constructor(public http: HttpClient, public loadingCtrl: LoadingController) {
    //Api  test
    this.apiUrl = "https://my-json-server.typicode.com/jonathanfalcon95/ApiTest";
  }

 
  getHeaderClear() {
    const httpOptions = {
      headers: new HttpHeaders({})
    };

    return httpOptions;
  }

  getModel(type: String, httpOptions = httpOptionsDefault) {
    return new Promise(resolve => {
      this.http.get(this.apiUrl + "/" + type, httpOptions).subscribe(
        data => {
          resolve(data);
        },
        err => {
          console.log(err);
        }
      );
    });
  }

  getModel_Id(id: String, tipo: String) {
    return new Promise(resolve => {
      this.http.get(this.apiUrl + "" + tipo + "/" + id).subscribe(
        data => {
          resolve(data);
        },
        err => {
          console.log(err);
        }
      );
    });
  }

  addModel(model, tipo: String, httpOptions = httpOptionsDefault) {
    return new Promise(resolve => {
      this.http.post(this.apiUrl + "" + tipo, model, httpOptions).subscribe(
        (data: any) => {
          console.log(data);

          resolve(data);
        },
        (err: any) => {
          console.log(err);
        }
      );
    });
  }

  updateModel(id, model, tipo: String, httpOptions = httpOptionsDefault) {
    return new Promise(resolve => {
      this.http
        .put(this.apiUrl + "" + tipo + "/" + id, model, httpOptions)
        .subscribe(
          (data: any) => {
            console.log(data);
            resolve(data);
          },
          (err: any) => {
            console.log(err);
          }
        );
    });
  }

  removeModel(id, tipo: String, httpOptions = httpOptionsDefault) {
    return new Promise(resolve => {
      this.http
        .delete(this.apiUrl + "" + tipo + "/" + id, httpOptions)
        .subscribe(
          (data: any) => {
            console.log(data);

            resolve(data);
          },
          (err: any) => {
            console.log(err);
          }
        );
    });
  }
}
